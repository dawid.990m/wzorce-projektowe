package codewars;

public class ReversedStrings {

    public String reverseString(String word) {
        char[] letters = word.toCharArray();
        char[] reversedWord = new char[letters.length];
        for(int i = 0; i < letters.length; i++) {
            reversedWord[i] = letters[letters.length - 1 - i];
        }
        return new String(reversedWord);
    }
}
