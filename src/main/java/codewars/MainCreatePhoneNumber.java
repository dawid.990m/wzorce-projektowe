package codewars;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class MainCreatePhoneNumber {

    public String createPhoneNumberBetter(int[] numbers) {
        String.format("(%d%d%d) %d%d%d-%d%d%d%d", java.util.stream.IntStream.of(numbers).boxed().toArray());

        return String.format("(%d%d%d) %d%d%d-%d%d%d%d",
                numbers[0], numbers[1], numbers[2],
                numbers[3], numbers[4],numbers[5],
                numbers[6],numbers[7],numbers[8],numbers[9]);
    }

    public String createPhoneNumber(int[] numbers) {
        String phoneNumber = "(";
        for(int i = 0; i < numbers.length; i++) {
            if(i == 2)
                phoneNumber = phoneNumber + numbers[i] + ") ";
            else if(i == 5)
                phoneNumber = phoneNumber + numbers[i] + "-";
            else
                phoneNumber = phoneNumber + numbers[i];
        }
        return phoneNumber;
    }
}
