package czynnosciowe.chainOfResponsibility.helper;

import java.util.Arrays;
import java.util.List;

public class ProgrammerHelper extends Helper{

    private static final List<String> errors = List.of(
      "Exception", "error", "outOfMemory"
    );

    @Override
    public void handle(String request) {
           if(Arrays.stream(request.split(" ")).anyMatch(errors::contains)){
               System.out.println("Odsyłam do programisty");
           }else{
               System.out.println("żądanie nie może być obsłużone, prosimy o kontakt z działem obsługi klienta");
           }
    }
}
