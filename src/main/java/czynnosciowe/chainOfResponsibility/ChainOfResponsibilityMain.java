package czynnosciowe.chainOfResponsibility;

import czynnosciowe.chainOfResponsibility.helper.ConsultantHelper;
import czynnosciowe.chainOfResponsibility.helper.FAQHelper;
import czynnosciowe.chainOfResponsibility.helper.Helper;
import czynnosciowe.chainOfResponsibility.helper.ProgrammerHelper;

//łańcuch zobowiązań
//celem jest delegowanie zadania do obiektu ktory jest odpowiedzialny za tego typu zadanie,
// zadania sa przetwarzane i przekazywane przez powiazane ze sobą obiekty
public class ChainOfResponsibilityMain {

    private static Helper getHelper(){
        Helper faqHelper = new FAQHelper();
        Helper consultantHelper = new ConsultantHelper();
        Helper programmerHelper = new ProgrammerHelper();

        faqHelper.setNextHelper(consultantHelper);
        consultantHelper.setNextHelper(programmerHelper);

        return faqHelper;
    }

    public static void main(String[] args) {
        Helper helper = getHelper();

        helper.handle("Otrzymuję informację, że hasło jest błędne");
        helper.handle("Komunikat BusinessException zbyt mała kwota");
        helper.handle("Exception brak pliku formularz.pdf");
    }
}
