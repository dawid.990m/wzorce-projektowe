package czynnosciowe.mediator;

public class User {
    private String name;
    private Mediator mediator;

    public User(String name, Mediator mediator){
        this.name = name;
        this.mediator = mediator;
    }

    public void receive(String message){
        System.out.println("Użytkownik " + name + " otrzymał wiadomość: " + message);
    }

    public void send(String message){
        mediator.send(message, this);
    }
}
