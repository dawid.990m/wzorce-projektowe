package czynnosciowe.mediator;
//pozwala na redukcje zaleznosci pomiedzy obiektami umozliwia zmniejszenie liczby powiązan miedzy roznymi klasami
// mediator jest jedyna klasa ktora dokladnie znma metody wszystkich inych klas ktorymi zarzadza
// nie muszą one nic o sobie wiedziec jedynie przekazuja polecenia mediatorowi a ten odsyla je do odpowiednich obiektow
public class MediatorMain {

    public static void main(String[] args) {

        Mediator mediator = new Mediator();

        User user1 = new User("John", mediator);
        User user2 = new User("Dawid", mediator);
        User user3 = new User("Bob", mediator);

        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);

        user1.send("cześć wszystkim!");

    }
}
