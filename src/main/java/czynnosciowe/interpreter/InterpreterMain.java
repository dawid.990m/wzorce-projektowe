package czynnosciowe.interpreter;

import java.util.ArrayList;
import java.util.List;

//dostarcza mozliwosc przetwarzania pewnego opisu gramatyki lub wyrazen
// jezyka poprzez tworzenie struktury bedacej w stanie interpretowac wyrazenia za pomoca zdefiniowanych reguł
public class InterpreterMain {

    public static void main(String[] args) {
        String roman = "CCLXXXVI";
        Context context = new Context(roman);

        List<Interpreter> interpreterList = new ArrayList<>();

        interpreterList.add(new ThousandInterpreter());
        interpreterList.add(new HundredInterpreter());
        interpreterList.add(new TenInterpreter());
        interpreterList.add(new OneInterpreter());

        interpreterList.forEach(interpreter -> interpreter.calculate(context));

        System.out.println("Liczba rzymska " + roman + " to: " + context.getArabicNumber());
    }
}
