package czynnosciowe.interpreter;

public abstract class Interpreter {

    public void calculate(Context context){
        if(context.getRomanNumber().length() == 0){
            return;
        }

        if(context.getRomanNumber().startsWith(nine())){
            context.setArabicNumber(context.getArabicNumber() + (9 * factor()));
            context.setRomanNumber(context.getRomanNumber().substring(2));
        } else if (context.getRomanNumber().startsWith(five())) {
            context.setArabicNumber(context.getArabicNumber() + (5 * factor()));
            context.setRomanNumber(context.getRomanNumber().substring(1));
        } else if (context.getRomanNumber().startsWith(four())) {
            context.setArabicNumber(context.getArabicNumber() + (4 * factor()));
            context.setRomanNumber(context.getRomanNumber().substring(2));
        }

        while (context.getRomanNumber().startsWith(one())) {
            context.setArabicNumber(context.getArabicNumber() + (1 * factor()));
            context.setRomanNumber(context.getRomanNumber().substring(1));
        }
    }

    public abstract String one();
    public abstract String four();
    public abstract String five();
    public abstract String nine();
    public abstract int factor();
}
