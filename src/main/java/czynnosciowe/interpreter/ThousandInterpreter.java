package czynnosciowe.interpreter;

public class ThousandInterpreter extends Interpreter{
    @Override
    public String one() {
        return "M";
    }

    @Override
    public String four() {
        return " ";
    }

    @Override
    public String five() {
        return " ";
    }

    @Override
    public String nine() {
        return " ";
    }

    @Override
    public int factor() {
        return 1000;
    }
}
