package czynnosciowe.visitor;

public class FloorVisitor implements Visitor{
    @Override
    public void visit(Building building) {
        System.out.println("number of floors in building: " + building.getFloor());
    }

    @Override
    public void visit(Bungalow bungalow) {
        System.out.println("number of floors in bungalow: " + bungalow.getFloor());
    }

    @Override
    public void visit(OfficeBuilding officeBuilding) {
        System.out.println("number of floors in officeBuilding: " + officeBuilding.getFloor());
    }

    @Override
    public void visit(SkyScraper skyScraper) {
        System.out.println("number of floors in skyScraper: " + skyScraper.getFloor());

    }
}
