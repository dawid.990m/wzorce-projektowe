package czynnosciowe.visitor;

//odwiedza dany obiekt w strukturze,
// obiekt powinien zaakceptowac wizytatora
// i pozwolic mu na wykonanie zadania w danym obiekcie
public class VisitorMain {

    public static void main(String[] args) {
        Building building = new Building();
        building.accept(new FloorVisitor());
    }
}
