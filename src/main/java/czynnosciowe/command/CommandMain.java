package czynnosciowe.command;

//zadanie wykonania okreslonej czynnosci jest opakowane w obiekt i przekazane dalej
//obiekty reprezentujacy zadanie moga byc kolejkowane i przekazywane do obiektu wywołującego
//poniewz mozemy miec wiele aktrywnych okien bedziemy kolejkowac zadania
// i wykonywac kolejne zadania z naszej kolejki
public class CommandMain {

    public static void main(String[] args) {
        Agent agent = new Agent();

        Window window1 = new Window("1");
        Window window2 = new Window("2");
        Window window3 = new Window("3");

        agent.addActivity(new OpenWindow(window1));
        agent.addActivity(new OpenWindow(window2));
        agent.startActivity();

        agent.addActivity(new CloseWindow(window1));
        agent.addActivity(new CloseWindow(window2));
        agent.addActivity(new OpenWindow(window3));
        agent.startActivity();

    }
}
