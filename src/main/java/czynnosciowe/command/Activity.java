package czynnosciowe.command;

public interface Activity {
    void execute();
}
