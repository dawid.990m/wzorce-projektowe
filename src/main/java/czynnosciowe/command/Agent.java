package czynnosciowe.command;

import java.util.ArrayList;
import java.util.List;

public class Agent {

    private List<Activity> activities = new ArrayList<>();

    public void addActivity(Activity activity){
        activities.add(activity);
    }
    public void startActivity(){
        activities.forEach(Activity::execute);
        activities.clear();
    }
}
