package czynnosciowe.command;

public class Window {
    private String identification;

    public Window(String identification) {
        this.identification = identification;
    }

    public void open(){
        System.out.println("okno " + identification + " zostało otwarte");
    }

    public void close(){
        System.out.println("okno " + identification + " zostało zamknięte");
    }
}
