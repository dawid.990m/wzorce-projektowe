package czynnosciowe.strategy;

public interface StrategyConverter {

    String convert(String file);
}
