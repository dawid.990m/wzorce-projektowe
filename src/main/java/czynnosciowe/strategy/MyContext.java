package czynnosciowe.strategy;

public class MyContext {
    private StrategyConverter strategyConverter;

    public MyContext(StrategyConverter strategyConverter) {
        this.strategyConverter = strategyConverter;
    }

    public String executeConversion(String file) {
        return strategyConverter.convert(file);
    }
}
