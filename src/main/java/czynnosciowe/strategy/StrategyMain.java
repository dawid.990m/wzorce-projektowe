package czynnosciowe.strategy;
//umorzliwia wybor odpowiedniego algorytmu w czasie działania programu
// w strastegi tworzymy obiekty które reprezentuja rozne strategie
//i odpowiedni dobierana jest poprzez kontekst programu
public class StrategyMain {
    public static void main(String[] args) {
        MyContext context = new MyContext(new PdfConverter());

        String newFile = context.executeConversion("file.txt");

        System.out.println("file after conversion: " + newFile);
    }
}
