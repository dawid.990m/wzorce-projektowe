package czynnosciowe.iterator;

import java.util.Iterator;

//zapewnia sekwencyjny dostep do zgrupowanych obiektów uzyskujemy
// w ten sposób dostęp do zawartosci obiektu bez ujawnienia jego wewnetrznej reprezentacji
public class IteratorMain {

    public static void main(String[] args) {
        Player player = new MusicPlayer();

        for(Iterator iterator = player.getIterator(); iterator.hasNext();) {
            String sample = (String) iterator.next();
            System.out.println(sample);
        }
    }
}
