package czynnosciowe.iterator;

import java.util.Iterator;

public interface Player {

    Iterator getIterator();
}
