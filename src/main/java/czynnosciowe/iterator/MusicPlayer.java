package czynnosciowe.iterator;

import java.util.Iterator;
import java.util.List;

public class MusicPlayer implements Player {

    private List<String> samples = List.of(
            "Bring me to life",
            "New Divide",
            "Nothing else matters"
    );

    @Override
    public Iterator getIterator() {
        return new SampleIterator(samples);
    }
}
