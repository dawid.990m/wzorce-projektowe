package czynnosciowe.Observer;

public class Subscriber extends Observer{

    public Subscriber(YoutubeChannel youtubeChannel) {
        this.youtubeChannel = youtubeChannel;
        youtubeChannel.addObserver(this);
    }

    @Override
    public void update() {
        System.out.println("Subscriber get info about new movie: " + youtubeChannel.getState());
    }
}
