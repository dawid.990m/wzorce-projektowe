package czynnosciowe.Observer;

//za za zadanie wysyłanie powiadomien do powiązanych obiektów
// o zmianie statusów powiązanych obiektów
public class ObserverMain {

    public static void main(String[] args) {
        YoutubeChannel youtubeChannel = new YoutubeChannel();
        new Subscriber(youtubeChannel);
        new Fanpage(youtubeChannel);
        new TwitterAccount(youtubeChannel);

        youtubeChannel.setState("NEW STATE!");
    }
}
