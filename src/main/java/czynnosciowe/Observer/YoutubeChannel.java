package czynnosciowe.Observer;

import java.util.ArrayList;
import java.util.List;

public class YoutubeChannel {

    private List<Observer> observers = new ArrayList<>();
    private String state;

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    private void notifyAllObservers() {
        observers.forEach(Observer::update);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyAllObservers();
    }


}
