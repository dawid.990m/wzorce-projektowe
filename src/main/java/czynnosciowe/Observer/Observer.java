package czynnosciowe.Observer;

public abstract class Observer {

    protected YoutubeChannel youtubeChannel;

    public abstract void update();
}
