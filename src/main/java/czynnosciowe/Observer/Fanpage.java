package czynnosciowe.Observer;

public class Fanpage extends Observer{

    public Fanpage(YoutubeChannel youtubeChannel) {
        this.youtubeChannel = youtubeChannel;
        youtubeChannel.addObserver(this);
    }

    @Override
    public void update() {
        System.out.println("Fanpage get info about new movie: " + youtubeChannel.getState());
    }
}
