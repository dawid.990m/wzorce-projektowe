package czynnosciowe.Observer;

public class TwitterAccount extends Observer{

    public TwitterAccount(YoutubeChannel youtubeChannel) {
        this.youtubeChannel = youtubeChannel;
        youtubeChannel.addObserver(this);
    }

    @Override
    public void update() {
        System.out.println("Twitter get info about new movie: " + youtubeChannel.getState());
    }
}
