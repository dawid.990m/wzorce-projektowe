package czynnosciowe.templateMethod;

public class UserTest extends Test {


    @Override
    public void initializeData() {
        System.out.println("Create data");
    }

    @Override
    public void checkTest() {
        System.out.println("check data");
    }

    @Override
    public void cleanData() {
        System.out.println("delete data");
    }
}
