package czynnosciowe.templateMethod;

import czynnosciowe.mediator.User;

//tworzymy szkielet aplikacji zawierająca oczekiwane zachowania
// a konkretna implementacja definiowana jest w metodach pochodnych
public class TemplateMethodMain {

    public static void main(String[] args) {
        Test test = new UserTest();
        test.run();
    }
}
