package czynnosciowe.state;

//uzależnia logikę od działania od stanu oibektu w jakim sie znajduje zmiana jego zachowania
// jest możliwa poprzez zmianę stanu wewnętrznego
public class StateMain {
    public static void main(String[] args) {
        MyContext context = new MyContext();

        SilentMode silentMode = new SilentMode();
        silentMode.notificationMode(context);

        context.notifyUser();

        LoudMode loudMode = new LoudMode();
        loudMode.notificationMode(context);

        context.notifyUser();
    }
}
