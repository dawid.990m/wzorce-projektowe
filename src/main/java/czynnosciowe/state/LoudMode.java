package czynnosciowe.state;

public class LoudMode implements PhoneModeState{
    @Override
    public void notificationMode(MyContext context) {
        context.setState(this);
    }

    public String toString() {
        return "Loud";
    }
}
