package czynnosciowe.state;

public class MyContext {

    private PhoneModeState state;

    public void setState(PhoneModeState state) {
        this.state = state;
    }

    public void notifyUser() {
        System.out.println(state.toString());
    }
}
