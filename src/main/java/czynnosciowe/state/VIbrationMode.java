package czynnosciowe.state;

public class VIbrationMode implements PhoneModeState{
    @Override
    public void notificationMode(MyContext context) {
        context.setState(this);
    }

    public String toString() {
        return "Vibration";
    }
}
