package czynnosciowe.state;

public class SilentMode implements PhoneModeState{
    @Override
    public void notificationMode(MyContext context) {
        context.setState(this);
    }

    public String toString() {
        return "No Reaction";
    }
}
