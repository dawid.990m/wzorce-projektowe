package czynnosciowe.momento;

//np. przycisk cofnij
public class MomentoMain {

    public static void main(String[] args) {
        Notepad notepad = new Notepad(new TextWindow());
        notepad.writeText("firs text \n");
        notepad.writeText("second text \n");

        notepad.save();
        notepad.writeText("third text \n");

        notepad.displayText();

        notepad.undo();
        notepad.displayText();
    }
}
