package czynnosciowe.momento;

public class TextWindow {

    private StringBuilder text;

    public TextWindow() {
        this.text = new StringBuilder();
    }

    public StringBuilder getText() {
        return text;
    }

    public void addText(String additionalText) {
        text.append(additionalText);
    }

    public Momento save(){
        return new Momento(text.toString());
    }

    public void rollback(Momento momento) {
        text = new StringBuilder(momento.getText());
    }
}
