package czynnosciowe.momento;

public class Momento {

    private String text;

    public String getText() {
        return text;
    }

    public Momento(String text) {
        this.text = text;
    }
}
