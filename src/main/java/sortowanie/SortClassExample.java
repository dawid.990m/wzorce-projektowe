package sortowanie;

public class SortClassExample {

    public int[] bubbleSort(int[] numbersArray) {
        for (int i = 0; i < numbersArray.length; i++) {
            for (int j = 1; j < numbersArray.length; j++) {
                if (numbersArray[j] < numbersArray[j - 1]) {
                    int biggest = numbersArray[j - 1];
                    numbersArray[j - 1] = numbersArray[j];
                    numbersArray[j] = biggest;
                }
            }
        }
        return numbersArray;
    }
}
