package kreacyjne.singleton;

//pozwala ograniczyć ilosc obiektów konkretnej klasy do jednej instancji
public class SingletonMain {
    public static void main(String[] args) {
        SingletonServer singletonServer = SingletonServer.getInstance();
        singletonServer.getResponse();

        SingletonServer singletonServer2 = SingletonServer.getInstance();

        //check the same reference
        System.out.println(singletonServer2 == singletonServer);
    }
}
