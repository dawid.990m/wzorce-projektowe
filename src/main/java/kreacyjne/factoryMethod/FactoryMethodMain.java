package kreacyjne.factoryMethod;

import kreacyjne.factoryMethod.transport.CargoTransport;
import kreacyjne.factoryMethod.transport.TransportFactory;

//pozwala na odzielenie wywołania obiektu od logiki jego tworzenia
//symulacja firmy transportowej, klient chce wyslac towar nie zalezy mu czym i jak,
// chce tylko wiedziec o czasie dostawy
public class FactoryMethodMain {

    public static void main(String[] args) {
        CargoTransport cargoTransport = TransportFactory.getTransport("truck");

        cargoTransport.process();
    }
}
