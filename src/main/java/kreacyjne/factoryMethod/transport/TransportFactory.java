package kreacyjne.factoryMethod.transport;

public class TransportFactory {

    private static final String TRUCK = "truck";
    private static final String PLANE = "plane";
    private static final String SHIP = "ship";

    public static CargoTransport getTransport(String transportName){
        CargoTransport cargoTransport = null;

        if(TRUCK.equalsIgnoreCase(transportName)) {
            cargoTransport = new Truck();
        } else if (PLANE.equalsIgnoreCase(transportName)) {
            cargoTransport = new Plane();
        } else if (SHIP.equalsIgnoreCase(transportName)) {
            cargoTransport = new Ship();
        }
        return cargoTransport;
    }
}
