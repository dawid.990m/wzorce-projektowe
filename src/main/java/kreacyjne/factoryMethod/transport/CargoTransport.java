package kreacyjne.factoryMethod.transport;

public interface CargoTransport {

    void process();
}
