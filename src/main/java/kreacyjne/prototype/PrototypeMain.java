package kreacyjne.prototype;

import java.util.HashMap;
import java.util.Map;
//wykorzystuje istniejący obiekt do stworzenia kolejnej instancji upraszcza
// to sposób tworzenie obiektów podobnych jest używany często do poprawy wydajności systemu
// ukrywa też sposob tworzenia obiektu koncowego, wykorzystywany w jednym z rodzajów bean springa @Prototype
public class PrototypeMain {
    public static void main(String[] args){

        Map<String, AnimalPrototype> animalsMap = new HashMap<>();

        animalsMap.put("Sheep", new Sheep("Chmurka", 20.4));
        animalsMap.put("Rabbit", new Rabbit("Bunny", 0.5));

        Sheep clonedSheep = (Sheep) animalsMap.get("Sheep").clone();
        clonedSheep.say();

        Rabbit clonedRabbit = (Rabbit) animalsMap.get("Rabbit").clone();
        clonedRabbit.say();

        System.out.println(animalsMap.get("Sheep") == clonedSheep);
    }
}
