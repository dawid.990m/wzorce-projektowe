package kreacyjne.builder;
//wzorzec upraszcza tworzenie skomplikowanych obiektów oddziela proces tworzenia obiektów
// od ich reprezentacji, obiekt tworzymy w wielu etapach dopiero
// po sprecyzowaniu ze obiekt jest ukonczony otrzymujemy jego instancję.
public class BuilderMain {
    public static void main(String[] args) {
        Car car = new CarBuilder().construct()
                .addChair("pasażera")
                .addChair("kierowcy")
                .engine("diesel 2.0")
                .model("skoda")
                .color("grey")
                .build();

        car.overview();
    }
}
