package kreacyjne.builder;

import java.util.ArrayList;
import java.util.List;

public class CarBuilder {

    private List<String> chairs = new ArrayList<>();
    private String model;
    private String engine;
    private String color;

    CarBuilder construct(){
        return new CarBuilder();
    }

    CarBuilder addChair(String chair){
        chairs.add(chair);
        return this;
    }

    CarBuilder model(String model){
        this.model = model;
        return this;
    }

    CarBuilder engine(String engine){
        this.engine = engine;
        return this;
    }

    CarBuilder color(String color){
        this.color = color;
        return this;
    }

    Car build(){
        return new Car(chairs, model, engine, color);
    }
}
