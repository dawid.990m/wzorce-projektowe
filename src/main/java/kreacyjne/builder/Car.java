package kreacyjne.builder;

import java.util.List;
import java.util.stream.Collectors;

public class Car {

    private List<String> chairs;
    private String model;
    private String engine;
    private String color;

    public Car(List<String> chairs, String model, String engine, String color) {
        this.chairs = chairs;
        this.model = model;
        this.engine = engine;
        this.color = color;
    }

    public void overview(){
        System.out.println(
                "Car elements: "
                + "\n chairs : " + chairs.stream().collect(Collectors.joining(", "))
                + "\n model : " + model
                + "\n engine : " + engine
                + "\n color : " + color
        );
    }
}
