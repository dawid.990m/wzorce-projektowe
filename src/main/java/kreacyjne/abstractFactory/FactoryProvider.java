package kreacyjne.abstractFactory;

public class FactoryProvider {

    static AbstractFactory getFactory(FactoryMode factoryMode){
        if(FactoryMode.PASSENGER == factoryMode){
            return new PassengerFactory();
        } else if(FactoryMode.CARGO == factoryMode){
            return new CargoFactory();
        }
        return null;
    }
}
