package kreacyjne.abstractFactory;

import kreacyjne.abstractFactory.transport.TransportType;

//rozszerzenie metody wytworczej firma transportowa zajmuje się dodatkowo ludzmi
public class AbstractFactoryMain {

    public static void main(String[] args) {
        System.out.println("Transport towarów będzie trwał: ");

        AbstractFactory cargoFactory = FactoryProvider.getFactory(FactoryMode.CARGO);
        cargoFactory.getTransport(TransportType.PLANE).process();

        System.out.println("Transport osób będzie trwał: ");

        AbstractFactory passengerFactory = FactoryProvider.getFactory(FactoryMode.PASSENGER);
        passengerFactory.getTransport(TransportType.PLANE).process();
    }
}
