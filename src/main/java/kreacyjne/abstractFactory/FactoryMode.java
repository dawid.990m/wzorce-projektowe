package kreacyjne.abstractFactory;

public enum FactoryMode {

    PASSENGER,
    CARGO
}
