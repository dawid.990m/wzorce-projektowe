package kreacyjne.abstractFactory;

import kreacyjne.abstractFactory.transport.Transport;
import kreacyjne.abstractFactory.transport.TransportType;

public abstract class AbstractFactory {

    abstract Transport getTransport(TransportType transportType);
}
