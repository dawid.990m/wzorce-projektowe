package kreacyjne.abstractFactory.transport;

public enum TransportType {

    BUS_TRUCK,
    PLANE,
    SHIP
}
