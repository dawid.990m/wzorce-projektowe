package kreacyjne.abstractFactory.transport;

public interface Transport {

    void process();
}
