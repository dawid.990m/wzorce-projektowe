package kreacyjne.abstractFactory.transport;

public class TransportFactory {

    private static final String BUS_TRUCK = "truck";
    private static final String PLANE = "plane";
    private static final String SHIP = "ship";

    public static Transport getTransport(String transportName){
        Transport cargoTransport = null;

        if(BUS_TRUCK.equalsIgnoreCase(transportName)) {
            cargoTransport = new CargoTruck();
        } else if (PLANE.equalsIgnoreCase(transportName)) {
            cargoTransport = new CargoPlane();
        } else if (SHIP.equalsIgnoreCase(transportName)) {
            cargoTransport = new CargoShip();
        }
        return cargoTransport;
    }
}
