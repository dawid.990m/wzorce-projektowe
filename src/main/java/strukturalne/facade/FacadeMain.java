package strukturalne.facade;

import strukturalne.facade.orderProcess.OrderFacade;
//ukrywa złozonosc systemu poprzez wystawienie uproszczonego i uporzadkowanego interfejsu
// z którego korzysta klient, fasada posiada dostep do systemu
// i w imieniu klienta wykonuje na nich zlozone operacxje
public class FacadeMain {
    public static void main(String[] args) {
        OrderFacade orderFacade = new OrderFacade();

        orderFacade.buy();

    }
}
