package strukturalne.facade.orderProcess;

public class OrderFacade {
    private BankAccount bankAccount = new BankAccount();
    private DataBase dataBase = new DataBase();
    private Warehouse warehouse = new Warehouse();
    private ParcelService parcelService = new ParcelService();

    public void buy(){
        bankAccount.moneyTransfer();
        dataBase.changeItemStatus();
        warehouse.packItem();
        parcelService.orderDelivery();
    }
}
