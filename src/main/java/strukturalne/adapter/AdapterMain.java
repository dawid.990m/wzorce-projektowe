package strukturalne.adapter;
//using when want to convert for example: euro -> dolar
public class AdapterMain {
    public static void main(String[] args) {

        AmazonItem amazonItem = new GofBook();
        Adapter adapter = new DollarAdapter(amazonItem);

        System.out.println("Cena Produktu: " + adapter.convert());

    }
}
