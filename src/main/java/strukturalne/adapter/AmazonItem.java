package strukturalne.adapter;

public interface AmazonItem {
    String getPrice();
}
