package strukturalne.adapter;

public interface Adapter {
    String convert();
}
