package strukturalne.composite;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private String name;
    private Integer age;
    private List<Person> children = new ArrayList<>();

    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
    }

    public List<Person> getChildren(){
        return children;
    }

    public String getName(){
        return name;
    }

    public void add(Person child){
        children.add(child);
    }

    public void remove(Person child){
        children.remove(child);
    }
}
