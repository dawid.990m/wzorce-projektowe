package strukturalne.composite;

import java.util.stream.Collectors;
//stworzenie struktury w ktorym wiele obiektow jest traktowanych tak samo jak pojedyncze obiekt
// najczesciej jest tworzona hierarchia obiektow tego samego typu obiekty te posiadaja podobne cechy
// i mozemy je traktowac w podobny sposób
public class CompositeMain {
    public static void main(String[] args) {
        Person father = new Person("Dawid", 30);
        Person child1 = new Person("Zenek", 6);
        Person child2 = new Person("Alicja", 4);

        father.add(child1);
        father.add(child2);

        System.out.println("Ojciec ma na imię: " + father.getName() +
                " a jego dzieci to: " +
                father.getChildren()
                        .stream()
                        .map(Person::getName)
                        .collect(Collectors.joining(", "))
        );
    }
}
