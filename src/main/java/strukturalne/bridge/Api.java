package strukturalne.bridge;

public interface Api {

    void send(String formattedMsg);
}
