package strukturalne.bridge;
//sluzy do oddzielenia abstrakcji obiektu od jego implementacji,
// tak aby 2 klasy były nioezalezne impl wzorca moze przypominac budowe adaptera
// z tym ze adapter sluzy do przeksztalłcenia dwoch niekompatybilnych interfejsow,
// a most oddziela abstrakcje od logiki obiektu.
public class BridgeMain {

    public static void main(String[] args) {
        Format format = new JsonFormat("Hello", new RestApi());
        format.sendMessage();
    }
}
