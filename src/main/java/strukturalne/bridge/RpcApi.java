package strukturalne.bridge;

public class RpcApi implements Api{

    @Override
    public void send(String formattedMsg) {
        System.out.println(formattedMsg + " została przesłana za pomocą RPC Api");
    }
}
