package strukturalne.decorator;

import java.util.List;

//dekoruje oryginalną klase dod funkcjonalnoscia dynamicznie podczs działania programu,
// pierwotny obiekt jest zwykle polem dekoratora jest on stosowany jako alternatywnie do dziedziczenia
//pozwala jednak na tworzenie obiektów dynamicznie nie tylko statycznie
//tworzone klasy sa mozliwie małe i ograniczamy ich odpowiedzialność
//Kompozycja ułatwia testowanie kodu oraz poprawia jego elastyczność
public class DecoratorMain {
    public static void main(String[] args) {

        Character character = new Character("Bruce","solider", 55, 30);
        PlayerDecorator playerDecorator = new ArmedPlayer(List.of("knife", "Shotgun","granade"), character);
        playerDecorator.playerSummary();
    }
}
