package strukturalne.decorator;

public abstract class PlayerDecorator {

    protected Character character;
    public abstract void playerSummary();

}
