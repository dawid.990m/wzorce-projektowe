package strukturalne.flyweight;

import java.util.Map;
import java.util.WeakHashMap;

public class SPaceShipFactory {
    private Map<String, SpaceShip> spaceShipCache = new WeakHashMap<>();

    public SpaceShip getSpaceShip(String type){
        return spaceShipCache.computeIfAbsent(type, SpaceShip::new);
    }

    public int cacheSize(){
        return spaceShipCache.size();
    }
}
