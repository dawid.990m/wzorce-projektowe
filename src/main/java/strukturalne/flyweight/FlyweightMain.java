package strukturalne.flyweight;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;
//stosuje sie w przypadku wielu instancji tej samej klasy ktor moga byc obslugiowane
// w ten sam sposob zadaniem jest redukcja wykorzystrania zasobow
// w tym celu obieklty korzystaja ze wspoldzielonych mniejszych elementow
public class FlyweightMain {
    public static void main(String[] args) {

        SPaceShipFactory factory = new SPaceShipFactory();
        IntStream.range(0, 10).forEach(i -> {
            SpaceShip spaceShip = factory.getSpaceShip("crusier");
            spaceShip.setPositionX(i);
            spaceShip.setPositiony(i);
            spaceShip.display();
        });

        IntStream.range(0, 10).forEach(i -> {
            SpaceShip spaceShip = factory.getSpaceShip("fighter3");
            spaceShip.setPositionX(ThreadLocalRandom.current().nextInt(i, 50));
            spaceShip.setPositiony(ThreadLocalRandom.current().nextInt(i, 100));
            spaceShip.display();
        });
        System.out.println("zostało stworzonych: " + factory.cacheSize() + " obiektów statków");
    }
}
