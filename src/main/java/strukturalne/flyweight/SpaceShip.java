package strukturalne.flyweight;

public class SpaceShip {
    private String type;
    private int positionX;
    private int positiony;

    public SpaceShip(String type){
        this.type = type;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositiony(int positiony) {
        this.positiony = positiony;
    }

    public void display(){
        System.out.println("statek kosmiczny " + type + "znajduje się na pozycji: " + positionX + " " + positiony);
    }
}
