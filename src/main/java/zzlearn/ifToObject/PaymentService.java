package zzlearn.ifToObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class PaymentService {

    public BigDecimal calculatePriceAndApplyDiscount(List<Product> products) {

        BigDecimal priceOfProducts = calculatePrice(products);
        int numberOfProducts = products.size();

//        if (numberOfProducts > 15) { //10% discount
//            return priceOfProducts.subtract(priceOfProducts
//                    .multiply(BigDecimal.valueOf((double) 10 / 100)));
//        } else if (numberOfProducts < 15 && numberOfProducts > 5) { // 5% discount
//            return priceOfProducts.subtract(priceOfProducts
//                    .multiply(BigDecimal.valueOf((double) 5 / 100)));
//        } else { // 1% discount
//            return priceOfProducts.subtract(priceOfProducts
//                    .multiply(BigDecimal.valueOf((double) 1 / 100)));
//        }

        Map<DiscountType, Rule<BigDecimal>> rules = createRules(priceOfProducts, numberOfProducts);
        return Stream.of(DiscountType.values())
                .filter(discountType -> rules.get(discountType).condition.get())
                .map(discountType -> rules.get(discountType).process.get())
                .findFirst()
                .orElse(null);
    }

    private Map<DiscountType, Rule<BigDecimal>> createRules(BigDecimal priceProducts, int numberOfProducts) {
        Map<DiscountType, Rule<BigDecimal>> rules = new HashMap<>();
        rules.put(DiscountType.TEN_PERCENT, createRuleForTenPercent(numberOfProducts, priceProducts));
        rules.put(DiscountType.FIVE_PERCENT, createRuleForFivePercent(numberOfProducts, priceProducts));
        rules.put(DiscountType.ONE_PERCENT, createRuleForOnePercent(numberOfProducts, priceProducts));

        return rules;
    }

    private Rule<BigDecimal> createRuleForTenPercent(int numberOfProducts, BigDecimal priceOfProducts) {
        return createRule(
                () -> numberOfProducts > 10,
                () -> priceOfProducts.subtract(priceOfProducts
                        .multiply(BigDecimal.valueOf((double) 10 / 100))));
    }

    private Rule<BigDecimal> createRuleForFivePercent(int numberOfProducts, BigDecimal priceOfProducts) {
        return createRule(
                () -> numberOfProducts < 10 && numberOfProducts > 5,
                () -> priceOfProducts.subtract(priceOfProducts
                        .multiply(BigDecimal.valueOf((double) 5 / 100))));
    }

    private Rule<BigDecimal> createRuleForOnePercent(int numberOfProducts, BigDecimal priceOfProducts) {
        return createRule(
                () -> numberOfProducts <= 5,
                () -> priceOfProducts.subtract(priceOfProducts
                        .multiply(BigDecimal.valueOf((double) 1 / 100))));
    }

    private Rule<BigDecimal> createRule(Supplier<Boolean> condition, Supplier<BigDecimal> process) {
        return new Rule<>(condition, process);
    }

    BigDecimal calculatePrice(List<Product> products) {
        BigDecimal sum = BigDecimal.ZERO;
        for (Product product : products) {
            sum = sum.add(product.getPrice());
        }
        return sum;
    }
}
