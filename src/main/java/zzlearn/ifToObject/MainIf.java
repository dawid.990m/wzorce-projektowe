package zzlearn.ifToObject;

import java.math.BigDecimal;
import java.util.List;

public class MainIf {

    public static void main(String[] args) {

        PaymentService paymentService = new PaymentService();
        List<Product> products = List.of(
                new Product(BigDecimal.valueOf(35.99), "Pillow"),
                new Product(BigDecimal.valueOf(599.99), "Bed"));


        paymentService.calculatePriceAndApplyDiscount(products);
    }
}
