package zzlearn.ifToObject;

public enum DiscountType {
    TEN_PERCENT,
    FIVE_PERCENT,
    ONE_PERCENT
}
