package codewars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReversedStringsTest {

    @Test
    public void testReverseWord() {

        ReversedStrings reversedStrings = new ReversedStrings();

        assertEquals("dlrow", reversedStrings.reverseString("world"));


    }

}