package codewars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class mainCreatePhoneNumberTest {

    @Test
    public void test() {
        MainCreatePhoneNumber mainCreatePhoneNumber = new MainCreatePhoneNumber();
        assertEquals("(123) 456-7890", mainCreatePhoneNumber.createPhoneNumberBetter(new int[] {1,2,3,4,5,6,7,8,9,0}));
    }


}