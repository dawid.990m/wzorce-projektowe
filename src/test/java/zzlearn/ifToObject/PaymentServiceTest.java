package zzlearn.ifToObject;



import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentServiceTest {

    PaymentService paymentService = new PaymentService();

    @Test
    public void calculatePriceAndApplyDiscountShouldReturnThreeProducts() {
        List<Product> products = List.of(
                new Product(BigDecimal.valueOf(35), "Pillow"),
                new Product(BigDecimal.valueOf(500), "Bed"),
                new Product(BigDecimal.valueOf(50), "Wooden ppon"));

        assertEquals(paymentService.calculatePrice(products), BigDecimal.valueOf(585));
    }

    @Test
    public void calculatePriceAndApplyDiscountShouldApplyDiscountAsOnePercentForFourProducts() {
        List<Product> products = List.of(
                new Product(BigDecimal.valueOf(35), "Pillow"),
                new Product(BigDecimal.valueOf(500), "Bed"),
                new Product(BigDecimal.valueOf(5), "Glass"),
                new Product(BigDecimal.valueOf(50), "Wooden ppon"));

        assertEquals(BigDecimal.valueOf(584.10).setScale(2, RoundingMode.CEILING), paymentService.calculatePriceAndApplyDiscount(products));
    }

    @Test
    public void calculatePriceAndApplyDiscountShouldApplyDiscountAsFivePercentForSixProducts() {
        List<Product> products = List.of(
                new Product(BigDecimal.valueOf(35), "Pillow"),
                new Product(BigDecimal.valueOf(500), "Bed"),
                new Product(BigDecimal.valueOf(250), "Carpet"),
                new Product(BigDecimal.valueOf(5), "Glass"),
                new Product(BigDecimal.valueOf(50), "Wooden ppon"),
                new Product(BigDecimal.valueOf(10), "Flower"));

        assertEquals(BigDecimal.valueOf(807.50).setScale(2, RoundingMode.CEILING), paymentService.calculatePriceAndApplyDiscount(products));
    }

    @Test
    public void calculatePriceAndApplyDiscountShouldApplyDiscountAsTenPercentForElevenProducts() {
        List<Product> products = List.of(
                new Product(BigDecimal.valueOf(35), "Pillow"),
                new Product(BigDecimal.valueOf(500), "Bed"),
                new Product(BigDecimal.valueOf(250), "Carpet"),
                new Product(BigDecimal.valueOf(5), "Glass"),
                new Product(BigDecimal.valueOf(50), "Wooden ppon"),
                new Product(BigDecimal.valueOf(35), "Pillow"),
                new Product(BigDecimal.valueOf(500), "Bed"),
                new Product(BigDecimal.valueOf(250), "Carpet"),
                new Product(BigDecimal.valueOf(5), "Glass"),
                new Product(BigDecimal.valueOf(50), "Wooden ppon"),
                new Product(BigDecimal.valueOf(10), "Flower"));

        assertEquals(BigDecimal.valueOf(1521).setScale(1, RoundingMode.CEILING), paymentService.calculatePriceAndApplyDiscount(products));
    }

}