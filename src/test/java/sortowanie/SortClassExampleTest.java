package sortowanie;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SortClassExampleTest {
    SortClassExample sortClassExample;

    @BeforeEach
    public void init() {
        sortClassExample = new SortClassExample();
    }

    @Test
    public void shouldSortArray_BubbleSort() {
        int[] array = new int[] {3, 5, 9, 1, 4, 2};

        assertEquals(Arrays.toString(new int[]{1, 2, 3, 4, 5, 9}), Arrays.toString(sortClassExample.bubbleSort(array)));

    }

}